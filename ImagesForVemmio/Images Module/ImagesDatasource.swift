//
//  ImagesDatasource.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation
import Alamofire

protocol ImagesDatasource {
    func load(completion: @escaping (_ images: [Image]?) -> ())
}


class SimpleImagesDatasource: ImagesDatasource {
    let parser: ImagesParser
    
    init(imagesParser: ImagesParser = SimpleImagesParser()) {
        self.parser = imagesParser
    }
    
    
    func load(completion: @escaping ([Image]?) -> ()) {
        Alamofire.request("https://dl.dropboxusercontent.com/u/16049878/images/test.json").responseJSON { [weak self] response in
            guard let rawJSON = response.result.value else { return }
            guard let imagesToParse = (rawJSON as? [String : AnyObject])?["images"] as? [[String : AnyObject]] else { return }
            guard let parsedImages = self?.parser.parse(from: imagesToParse) else { return }
            completion(parsedImages)
        }
    }
}
