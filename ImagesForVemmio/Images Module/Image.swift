//
//  Image.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation
import UIKit

class Image {
    var imageURL: URL
    // I assumed that "timestamp" and "description" can be sometimes nil as it is in returned json.
    var timestamp: Date?
    var description: String?
    private var image: UIImage?
    
    init(imageURL: URL, timestamp: Date?, description: String?) {
        self.imageURL = imageURL
        self.timestamp = timestamp
        self.description = description
    }
    
    
    func load(completion: @escaping ((_ image: UIImage?) -> ())) {
        if let existingImage = image {
            completion(existingImage)
        } else {
            downloadAsynchronously(with: completion)
        }
    }
    
    
    private func downloadAsynchronously(with completion: @escaping ((_ image: UIImage) -> ())) {
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: self.imageURL) else { return }
            guard let downloadedImage = UIImage(data: imageData) else { return }
            self.image = downloadedImage
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
        }
    }
}
