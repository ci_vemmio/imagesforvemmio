//
//  ImagesParser.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation

protocol ImagesParser {
    func parse(from images: [[String : AnyObject]]) -> [Image]
}


class SimpleImagesParser: ImagesParser {
    func parse(from images: [[String : AnyObject]]) -> [Image] {
        var parsedImages = [Image]()
        for imageDictionary in images {
            guard let imageStringURL = imageDictionary["imageURL"] as? String, let imageURL = URL(string: imageStringURL) else { break }
            let imageTimestamp = (imageDictionary["info"]?["timestamp"] as? String)?.date()
            let imageDescription = imageDictionary["info"]?["description"] as? String
            
            let image = Image(imageURL: imageURL, timestamp: imageTimestamp, description: imageDescription)
            parsedImages.append(image)
        }
        
        return parsedImages
    }
}
