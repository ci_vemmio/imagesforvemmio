//
//  ImagesInteractor.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation

class SimpleImagesInteractor {
    var view: ImagesView
    var datasource: ImagesDatasource
    
    init(view: ImagesView, datasource: ImagesDatasource = SimpleImagesDatasource()) {
        self.view = view
        self.datasource = datasource
        reloadImages()
    }
}


private extension SimpleImagesInteractor {
    func reloadImages() {
        datasource.load { images in
            guard let existingImages = images else { return }
            self.view.presentImages(images: existingImages)
        }
    }
}
