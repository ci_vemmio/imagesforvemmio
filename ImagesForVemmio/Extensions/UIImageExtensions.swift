//
//  UIImageExtensions.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func masked(by maskImage: UIImage) -> UIImage? {
        guard let selfReference = self.cgImage else { return nil }
        guard let maskReference = maskImage.cgImage else { return nil }
        guard let dataProviderForMask = maskReference.dataProvider else { return nil }
        guard let cgMask = CGImage(maskWidth: maskReference.width, height: maskReference.height, bitsPerComponent: maskReference.bitsPerComponent, bitsPerPixel: maskReference.bitsPerPixel, bytesPerRow: maskReference.bytesPerRow, provider: dataProviderForMask, decode: nil, shouldInterpolate: true) else { return nil }
        guard let maskedCGImage = selfReference.masking(cgMask) else { return nil }
        return UIImage(cgImage: maskedCGImage)
    }
}
