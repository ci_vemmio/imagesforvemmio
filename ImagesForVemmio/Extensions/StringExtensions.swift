//
//  StringExtensions.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation

extension String {
    func date(format: String = "yyyy-MM-dd'T'HH:mm:ssZZZ") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}
