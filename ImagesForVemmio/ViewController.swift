//
//  ViewController.swift
//  ImagesForVemmio
//
//  Created by Krystian Sęk on 10/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import UIKit

protocol ImagesView {
    func presentImages(images: [Image])
}

class ViewController: UICollectionViewController {
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    private var interactor: SimpleImagesInteractor?
    fileprivate var images = [Image]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor = SimpleImagesInteractor(view: self)
    }
    
    
    fileprivate func presentAlert(for selectedImage: Image) {
        guard let timestamp = selectedImage.timestamp else { return }
        var alertMessage = "Timestamp:\n\(timestamp)"
        
        // I assumed that "description" property sometimes can be nil (as it is in json example)
        if let description = selectedImage.description { alertMessage += "\n\nDescription:\n\(description)" }
        
        let alert = UIAlertController(title: "Image Details", message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}


extension ViewController: ImagesView {
    func presentImages(images: [Image]) {
        loadingIndicator.stopAnimating()
        self.images = images
        collectionView?.reloadData()
    }
}


extension ViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCell
        cell.setup(with: images[indexPath.item])
        return cell
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = images.remove(at: sourceIndexPath.item)
        images.insert(item, at: destinationIndexPath.item)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedImage = images[indexPath.item]
        presentAlert(for: selectedImage)
    }
}


class ImageCell: UICollectionViewCell {
    var image: Image?
    @IBOutlet weak var viewWithImage: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    func setup(with image: Image) {
        self.image = image
        image.load { [weak self] (image) in
            guard let maskImage = UIImage(named: "ImageMask") else { return }
            self?.viewWithImage.image = image?.masked(by: maskImage)
            self?.loadingIndicator.stopAnimating()
        }
    }
}
