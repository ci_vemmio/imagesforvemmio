# Task
```
Create an app that downloads a JSON file from: 
https://dl.dropboxusercontent.com/u/16049878/images/test.json
 
1) The JSON file contains a list of images, present those images in a view, 
2) The view's background color should be #FB8C00 
3) The images should be masked using the attached "star" image 
4) Tapping the image presents an alert that will display the image's timestamp and description.
5) The user should be able to drag the images around the view. 
6) All layout should be done using Autolayout 
```

## Dependency management
Project uses *Carthage* to fetch _Alamofire_. 

Please make sure you already have Carthage installed before trying to run this project.